class Tamagotchi():
	"""docstring for Tamagotchi"""
	def __init__(self,name):
		self.name = name
		self.hunger = 50
		self.fullness = 50
		self.happiness = 0
		self.tiredness = 0
		self.mood = ":)"
		self.live = "DEAD"

	def feed(self):
		self.hunger -=10
		self.fullness +=10
		self.moody()
		self.inspect()



	def played_with(self):
		self.happiness +=10
		self.tiredness +=10
		self.inspect()



	def goes_to_sleep(self):
		self.tiredness -= self.tiredness//2
		self.inspect()


	def poo_poo(self):
		self.fullness -=10
		self.inspect()


	def time_passess(self):
		self.hunger +=5
		self.tiredness +=5
		self.happiness -=5
		self.moody()
		self.dies()
		self.inspect()


	def moody(self):
		self.mood = ":(" if self.hunger > 70 else ":)"


	def dies(self):
		self.live = "DEAD" if self.hunger >= 100 else "ALIVE"


	def inspect(self):
		print({'Hunger':self.hunger,'Fullness': self.fullness,'Happiness':self.happiness, 'Tiredness':self.tiredness,"Mood": self.mood, "Lives": self.live})



