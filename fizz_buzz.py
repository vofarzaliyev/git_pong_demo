def fizz_buzz():
    while True:
        try:
            user_input = int(input("Please enter an integer to be converted: "))
            break
        except ValueError:
            pass
    result = ""
    result = result.join("Fizz"*(user_input%3==0)+"Buzz"*(user_input%5==0)+"Pop"*(user_input%7==0))
    return user_input if result == "" else result

while True:
    print(fizz_buzz())
